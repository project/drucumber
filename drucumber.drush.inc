<?php

/**
 * Implementation of hook_drush_command().
 */
function drucumber_drush_command() {
  $command = array();

  $command['drucumber compile'] = array(
    'callback' => '_drucumber_compile',
    'description' => 'Compiles a feature file into test',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'filename' => 'Feature file to compile',
    ),
    'aliases' => array(
      'dc'
    ),
    'examples' => array(
      // TOOD
    ),
  );

  return $command;
}

/**
 * Loads the required includes.
 */
function drucumber_load_rt() {
  module_load_include('module', 'drucumber');
  drucumber_init();
}

/**
 * Callback function for the "drucumber compile" command.
 *
 * @todo figure out something on the path bug.
 *
 * @param string $filename
 */
function _drucumber_compile($filename) {
  drucumber_load_rt();
  $spec = file_get_contents($filename);
  file_put_contents($filename . '.test', drucumber_compile($spec));
}
