<?php

function drucumber_drucumber_keyword_feature() {
  return array('##' => 'drucumber_render_feature');
}

function drucumber_render_feature() {
  $args = func_get_args();
  $args = drucumber_render_normalize_args($args);
  return <<<EOD
class {$args['name']} extends DrupalWebTestCase {
  {$args['text']}
}
EOD;
}

function drucumber_drucumber_keyword_info() {
  return array('##' => 'drucumber_render_info');
}

function drucumber_render_info($text) {
  return <<<EOD
  public static function getInfo() {
    return array(
      $text
      'group' => 'Drucumber',
    );
  }
EOD;
}

function drucumber_drucumber_keyword_name() {
  return array('##' => 'drucumber_render_name');
}

function drucumber_render_name($name) {
  return <<<EOD
     'name' => '$name',
EOD;
}

function drucumber_drucumber_keyword_description() {
  return array('##' => 'drucumber_render_description');
}

function drucumber_render_description($description) {
  return <<<EOD
     'description' => '$description',
EOD;
}

function drucumber_drucumber_keyword_background() {
  return array('##' => 'drucumber_render_background');
}

function drucumber_render_background($text) {
  return <<<EOD
  public function setUp() {
    parent::setUp();
    $text
  }
EOD;
}

function drucumber_drucumber_keyword_given() {
  return array(
    '#([a-zA-Z ]*)user with ([a-zA-Z \,]*) role#i' => 'drucumber_render_given_user_with__role'
  );
}

function drucumber_render_given_user_with__role($user, $roles) {
  $roles = drucumber_render_comma_separated_string_to_array_definition($roles);
  $user = str_replace(' ', '_', $user);
  if(strlen($user) > 0) {
    $user .= '_';
  }
  return <<<EOD
    \${$user}user = \$this->drupalCreateUser({$roles});
    \$this->drupalLogin(\${$user}user);
EOD;
}

function drucumber_drucumber_keyword_scenario() {
  return array('##' => 'drucumber_render_scenario');
}

function drucumber_render_scenario() {
  $args = func_get_args();
  $args = drucumber_render_normalize_args($args);
  $args['name'] = ucfirst($args['name']);
  return <<<EOD
  public function test{$args['name']}() {
    {$args['text']}
  }
EOD;
}

function drucumber_drucumber_keyword_should_see() {
  return array('#link to ([a-zA-Z_\/]*)#i' => 'drucumber_render_should_see_link_to');
}

function drucumber_render_should_see_link_to($link) {
  return <<<EOD
    \$this->assertRaw('href="$link"', 'The specified link ($link) does not exists.');
EOD;
}
