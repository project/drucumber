<?php

/**
 * @file
 *
 * This file provides non-working PHP code as an additional documentation
 * for doxygen, and the documentation of the hooks which are provided by
 * the module.
 */

/**
 * @mainpage Drucumber API manual
 *
 * @ref drucumber_static_hooks
 * @ref drucumber_dynamic_hooks
 * @ref drucumber_helper_functions
 */

/**
 * @defgroup drucumber_static_hooks Static hooks
 * @{
 *
 * These are the static hooks which are provided by drucumber.
 */

/**
 * This hook runs after the spyc extracted the AST from the raw source.
 *
 * By implementing this hook, you can alter the contents of the AST.
 * It can be helpful if you want to do some transformations (like you
 * created a DSL somewhere).
 *
 * Note that if a keyword becomes nested, like this:
 * <code>foo: bar</code>
 * becomes
 * <code>foo:
 *   - bar
 *   - baz</code>
 * It will change the AST from:
 * <code>"foo" => "bar",</code>
 * to:
 * <code>"foo" => array("bar", "baz"),</code>
 *
 * This will be workarounded later @see drucumber_dynamic_hooks
 *
 * @param array $ast
 */
function hook_drucumber_ast_alter(&$ast) {}

/**
 * This hook lets you to modify the generated PHP source.
 *
 * @param string $test
 */
function hook_drucumber_test_alter(&$test) {}

/**
 * @}
 */

/**
 * @defgroup drucumber_dynamic_hooks Dynamic hooks or keyword hooks
 * @{
 *
 * In order to "register" a keyword, you have to implement the drucumber_keyword_* hook.
 * The hook name is based on the keyword. So if you have something like this:
 * <code>fookeyword: do bar on ...</code>
 * then the hook will be invoked like this:
 * <code>hook_drucumber_keyword_fookeyword('do bar on ...')</code>
 * If you have a keyword with multiple words, then the words can become arguments.
 * Example:
 * <code>foo bar: baz</code>
 * Invoked hooks:
 * <code>
 * hook_drucumber_keyword_foo_bar('baz')
 * hook_drucumber_keyword_foo('bar', 'baz')
 * </code>
 * This behavior can be inspected more deeply if you read drucumber_get_callback_provider().
 *
 * The hooks must return a keyed array of callbacks (which will render the actual content), where
 * the keys are regexes, which will be matched against the value of the keyword with preg_match().
 * 
 * Good to know: if your keyword becomes nested, like this:
 * <code>foo: bar</code>
 * becomes
 * <code>foo:
 *   - bar
 *   - baz</code>
 * Then the ast will become a bit messy. There is a hack in _drucumber_ast_to_test() to workaround this.
 *
 * Tip: if you want to use the multi-keyword dispatch, you should take a quick look on the helper functions:
 * - drucumber_render_normalize_args($args)
 */

/**
 * @}
 */

/**
 * @defgroup drucumber_helper_functions Helper functions for drucumber
 * @{
 */

/**
 * @}
 */
