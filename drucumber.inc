<?php

/**
 * Converts a feature specification into a simpletest test.
 *
 * @param string $spec
 * @return string
 */
function drucumber_compile($spec) {
  return drucumber_ast_to_test(drucumber_get_ast($spec));
}

/**
 * Returns a callback for a keyword and a text.
 *
 * @param string $keyword
 * @param string $string
 * @return array
 */
function drucumber_get_callback_provider($keyword, $string) {
  for($keywords = explode(' ', strtolower($keyword)), $extra = array(); count($keywords) > 0; array_unshift($extra, array_pop($keywords))) {
    foreach(module_invoke_all('drucumber_keyword_' . implode('_', $keywords)) as $regex => $callback) {
      $matches = array();
      if(preg_match($regex, $string, $matches)) {
        array_shift($matches);
        return array_merge(array($callback), $extra, $matches);
      }
    }
  }
  return NULL;
}

/**
 * Extracts an abstract syntax tree from a source file.
 *
 * @param string $text
 * @return array
 */
function drucumber_get_ast($text) {
  if(!class_exists('Spyc')) return NULL;
  $ast = Spyc::YAMLLoadString($text);
  drupal_alter('drucumber_ast', $ast);
  return $ast;
}

/**
 * Renders the AST into PHP code.
 *
 * @param array $ast
 * @return string
 */
function drucumber_ast_to_test($ast) {
  $test = _drucumber_ast_to_test($ast);
  drupal_alter('drucumber_test', $test);
  return $test;
}

/**
 * Recursive helper function for drucumber_ast_to_test().
 *
 * @param array $ast
 * @param string $parent
 * @return string
 */
function _drucumber_ast_to_test($ast, &$parent = NULL) {
  $test = '';
  foreach($ast as $keyword => $node) {
    if(is_numeric($keyword)) {
      // This : hack is required in order to render
      // the code correctly, when a keyword becomes
      // nested.
      if(strpos($parent, ':') === FALSE) {
        $parent = ':' . $parent;
      }
      $keyword = ltrim($parent, ':');
    }
    $text = is_array($node) ? _drucumber_ast_to_test($node, $keyword) : $node;
    if(strpos($keyword, ':') === 0) {
      $test .= $text;
    } else {
      $args = drucumber_get_callback_provider($keyword, $text);
      if(is_array($args)) {
        $callback = array_shift($args);
        array_push($args, $text);
        if(function_exists($callback)) {
          $test .= call_user_func_array($callback, $args);
        }
      }
    }
  }
  return $test;
}

/**
 * Convert the arg list into a camelcase name
 * and a text.
 *
 * This function is useful when you have to
 * generate classnames or function names from
 * the keyword.
 *
 * @param array $args
 * @return array
 */
function drucumber_render_normalize_args($args) {
  $text = array_pop($args);
  $name = '';
  foreach($args as $a) {
    $name .= ucfirst($a);
  }
  return array(
    'name' => $name,
    'text' => $text,
  );
}

/**
 * Converts a comma-separated string into an array definition.
 *
 * @param string $string
 * @return string
 */
function drucumber_render_comma_separated_string_to_array_definition($string) {
  $array = explode(',', $string);
  $array = array_map('trim', $array);
  $out = 'array(';
  foreach($array as $a) {
    $out .= "'$a',";
  }
  $out .= ')';
  return $out;
}

/**
 * Checks if a PHP snippet is syntactically correct.
 *
 * @param string $source
 * @return bool
 */
function drucumber_check_syntax($source) {
  return (bool)@eval("return true;\n$source");
}
